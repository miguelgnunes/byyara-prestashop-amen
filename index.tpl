{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($HOOK_HOME_TAB_CONTENT) && $HOOK_HOME_TAB_CONTENT|trim}
    {if isset($HOOK_HOME_TAB) && $HOOK_HOME_TAB|trim}
        <ul id="home-page-tabs" class="nav nav-tabs clearfix">
			{$HOOK_HOME_TAB}
		</ul>
	{/if}
	<div class="tab-content">{$HOOK_HOME_TAB_CONTENT}</div>
{/if}
{if isset($HOOK_HOME) && $HOOK_HOME|trim}
	<div class="slider-section">
		<div id="home-mobile">
			<img src="{$img_dir}backgrounds/mobilehome.jpg">
			<div>
				<center>
					<img style="width: 30%; margin-bottom: 15px" src="{$img_dir}logoPretoFundoT.png">
					<a href="index.php?controller=authentication#login_form">
						<button class="btn btn-default button button-medium exclusive myMiddleActionButton" style="display: block; width: 170px">
							<span class="myMiddleActionButton-text myMiddleActionButton-text-right">
								<!-- <i class="icon-user left"></i> -->
								{l s='SIGN IN'}
							</span>
							<span class="glyphicon glyphicon-chevron-right middlebuttonarrow middlebuttonarrow-right" aria-hidden="true"></span>
						</button>
					</a>
					<a href="index.php?controller=authentication">
						<button class="btn btn-default button button-medium exclusive myMiddleActionButton" style="display: block; width: 170px; margin-top: 12px">
							<span class="myMiddleActionButton-text myMiddleActionButton-text-right">
								<!-- <i class="icon-user left"></i> -->
								{l s='REGISTER'}
							</span>
							<span class="glyphicon glyphicon-chevron-right middlebuttonarrow middlebuttonarrow-right" aria-hidden="true"></span>
						</button>
					</a>
					<a href="index.php?id_category=12&controller=category" style="text-decoration: none">
						<button class="button btn btn-default button-medium myFinalizeButton" style="display: block; width: 170px; margin-top: 12px">
							<span class="myFinalizeButton-text" style="color: black !important; text-shadow: none">
								<!-- <i></i> -->
								{l s='BY YARA SHOP'}
							</span>
						</button>
					</a>
					<div id="languages-block-top-mobile-home" class="languages-block" style="background-color: transparent !important; float: none !important; margin-top: 20px; width: 100% !important"> 
							{foreach from=$languages key=k item=language name="languages"}
								{if $language.iso_code == $lang_iso}
										<h4 style="display:inline; font-weight: 600">{$language.iso_code|regex_replace:"/\s\(.*\)$/":""|upper}</h4>
										{* <span style="color: white">|</span> *}
								{/if}
							{/foreach}
								{foreach from=$languages key=k item=language name="languages"}
										{if $language.iso_code != $lang_iso}
												{assign var=indice_lang value=$language.id_lang}
												{if isset($lang_rewrite_urls.$indice_lang)}
													<a href="{$lang_rewrite_urls.$indice_lang|escape:'html':'UTF-8'}" title="{$language.name}">
												{else}
													<h4 style="display: inline; margin: 0 5px">|</h3>
													<a href="{$link->getLanguageLink($language.id_lang)|escape:'html':'UTF-8'}" title="{$language.name}">
												{/if}
													<h4 style="display: inline">{$language.iso_code|regex_replace:"/\s\(.*\)$/":""|upper}</h4>
													<!-- <h3 style="display: inline">{$language.name|regex_replace:"/\s\(.*\)$/":""}</h3> -->
												</a>
									{/if}
								{/foreach}
						</div>
				</center>
			</div>
		</div>
		{$HOOK_HOME}</div>
{/if}