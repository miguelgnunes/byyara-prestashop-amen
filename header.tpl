{*
	* 2007-2015 PrestaShop
	*
	* NOTICE OF LICENSE
	*
	* This source file is subject to the Academic Free License (AFL 3.0)
	* that is bundled with this package in the file LICENSE.txt.
	* It is also available through the world-wide-web at this URL:
	* http://opensource.org/licenses/afl-3.0.php
	* If you did not receive a copy of the license and are unable to
	* obtain it through the world-wide-web, please send an email
	* to license@prestashop.com so we can send you a copy immediately.
	*
	* DISCLAIMER
	*
	* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
	* versions in the future. If you wish to customize PrestaShop for your
	* needs please refer to http://www.prestashop.com for more information.
	*
	*  @author PrestaShop SA <contact@prestashop.com>
	*  @copyright  2007-2015 PrestaShop SA
	*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
	*  International Registered Trademark & Property of PrestaShop SA
	*}
	<!DOCTYPE HTML>
	<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{$language_code|escape:'html':'UTF-8'}"><![endif]-->
	<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$language_code|escape:'html':'UTF-8'}"><![endif]-->
	<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$language_code|escape:'html':'UTF-8'}"><![endif]-->
	<!--[if gt IE 8]> <html class="no-js ie9" lang="{$language_code|escape:'html':'UTF-8'}"><![endif]-->
	<html lang="{$language_code|escape:'html':'UTF-8'}">
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
		{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		{if isset($css_files)}
		{foreach from=$css_files key=css_uri item=media}
		<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
		{/foreach}
		{/if}
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
		{$js_def}
		{foreach from=$js_files item=js_uri}
		<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
		{/foreach}
		{/if}
		{$HOOK_HEADER}
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin,latin-ext" type="text/css" media="all" />
		<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}" style="opacity: 0">
	{if !isset($content_only) || !$content_only}
	{if isset($restricted_country_mode) && $restricted_country_mode}
	<div id="restricted-country">
		<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
	</div>
	{/if}
	<div id="page">
		{if $page_name !='index'}
			<div id="fullscreen-menu-container">
				<div id="menu-icon-container">
					<img id="menu-icon" style="height: 70%" src="{$img_dir}mobile-menu-icon.png" onclick="menumobileclick()">
					<a href="{$base_dir}index.php?controller=order" title="{l s='View my shopping cart' mod='blockcart'}" rel="nofollow">
						<div style="position: relative; display: inline-block; height: 20px; width: 30px; margin-left: 30%">
							<img style="height: 100%" src="{$img_dir}icons/mobile-bag.png">
							<span style="color: white !important; position: absolute; line-height: 11px; bottom: 0; right: 0">{$cart_qties}</span>
						</div>
					</a>
					<a href="{$base_dir}index.php"><img id="menu-icon" style="height: 70%; float: right" src="{$img_dir}logobyyara-02.png"></a>
				</div>
				<div id="menu-items-container">
					{* <div style="display: table; width: 100%; height: 20%">
						{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
					</div> *}
					<div style="display: table; width: 100%; height: 60% ">
						<div style="display: table-cell; vertical-align: middle">
							<a href="index.php?id_cms=4&controller=cms"><h3>{l s='ABOUT US'}</h3></a>
							<a href="index.php?id_category=12&controller=category"><h3>{l s='JEWELRY'}</h3></a>
							<a href="index.php?controller=my-account"><h3>{l s='MY ACCOUNT'}</h3></a>
							<a href="index.php?id_cms=6&controller=cms&id_lang=1"><h3>{l s='CONTACT US'}</h3></a>
						</div>
					</div>
					<div style="display: table; width: 100%; height: 40%">
						<div style="height: 20px">
						{if $logged}
							<a href="index.php?mylogout">
								<img src="{$img_dir}icons/mobile-perfillogin.png" style="height: 100%">
								<span style="font-size: 18px; color: white !important">{l s='LOGOUT' mod='blockcart'}</span>
							</a>
						{else}
							<a href="index.php?controller=my-account#login_form">
								<img src="{$img_dir}icons/mobile-perfillogout.png" style="height: 100%">
								<span style="font-size: 18px; color: white !important">{l s='Sign in'}</span>
							</a>
						{/if}
						</div>


						<div id="languages-block-top" class="languages-block" style="background-color: transparent !important; float: none !important; margin-top: 20px; width: 100% !important"> 
							{foreach from=$languages key=k item=language name="languages"}
								{if $language.iso_code == $lang_iso}
										<h4 style="display:inline; font-weight: 600">{$language.iso_code|regex_replace:"/\s\(.*\)$/":""|upper}</h4>
										{* <span style="color: white">|</span> *}
								{/if}
							{/foreach}
								{foreach from=$languages key=k item=language name="languages"}
										{if $language.iso_code != $lang_iso}
												{assign var=indice_lang value=$language.id_lang}
												{if isset($lang_rewrite_urls.$indice_lang)}
													<a href="{$lang_rewrite_urls.$indice_lang|escape:'html':'UTF-8'}" title="{$language.name}">
												{else}
													<h4 style="display: inline; margin: 0 5px">|</h3>
													<a href="{$link->getLanguageLink($language.id_lang)|escape:'html':'UTF-8'}" title="{$language.name}">
												{/if}
													<h4 style="display: inline">{$language.iso_code|regex_replace:"/\s\(.*\)$/":""|upper}</h4>
													<!-- <h3 style="display: inline">{$language.name|regex_replace:"/\s\(.*\)$/":""}</h3> -->
												</a>
									{/if}
								{/foreach}
						</div>


					</div>
				</div>
			</div>
		{/if}

		<header id="header">
			{* <div class="banner">
				<div class="my-container">
					<div class="row">
						{hook h="displayBanner"}
					</div>
				</div>
			</div>
			<div class="nav">
				<div class="my-container">
					<div class="row">
						<nav>{hook h="displayNav"}</nav>
					</div>
				</div>
			</div> *}
			<div class="row">
				<div id="header_logo_byyara" class="col-xs-2">
					<a href="{if $force_ssl}{$base_dir_ssl}{else}{$base_dir}index.php{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
						<img class="logo-image img-responsive" src="{$img_dir}byyaralogo.png?{$img_update_time}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
					</a>
				</div>
				<div id="header-options-container" class="col-xs-10">
					<div style="float: right">
						{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
					</div>
					{* <div id="top-menu">
						<div class="menu-item">{l s='ABOUT US'}</div>
						<div class="menu-item">
							<a href="{$base_dir}12-pulseiras">{l s='JEWELRY'}</a>
						</div>
						<div class="menu-item">
							<a href="{$base_dir}a-minha-conta">{l s='MY ACCOUNT'}</a>
						</div>
						<div class="menu-item">{l s='CONTACT US'}</div>
					</div> *}
				</div>
			</div>
		</header>
	{if $smarty.get.controller != "index"}
		<div class="content-container">
			<div class="columns-container">
				{* {if $page_name != "product"} 
					<div class="category-heading-image-container">
	            		<img class="category-heading-image" src="{$img_dir}pattern-page.png?{$img_update_time}">
	        		</div>
        		{/if} *}
				<div id="columns" class="my-container">
					{if $page_name !='index' && $page_name !='pagenotfound'}
						{if $page_name == "product"} 
						<div class="row">
							{include file="$tpl_dir./breadcrumb.tpl"}
						</div>
						{/if}
					{/if}
					<div id="slider_row" class="row">
						<div id="top_column" class="center_column col-xs-12 col-sm-12">
							{hook h="displayTopColumn"}</div>
					</div>
					<div class="row" style="height: 100%">
						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}">
							{/if}
	{/if}
