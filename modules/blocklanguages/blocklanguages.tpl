{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{* languages and currency container  *}
<div id="language-currency-container">

	<!-- Block languages module -->
	{if count($languages) > 1}
		<div id="languages-block-top" class="languages-block">
			<span id="languagetext">{l s='LANGUAGE' mod='blocklanguages'}</span>
			{foreach from=$languages key=k item=language name="languages"}
				{if $language.iso_code == $lang_iso}
						<h4 style="display:inline">{$language.iso_code|regex_replace:"/\s\(.*\)$/":""|upper}</h3>
						<!-- <h3 style="display:inline">{$language.name|regex_replace:"/\s\(.*\)$/":""}</h3> -->
				{/if}
			{/foreach}
				{foreach from=$languages key=k item=language name="languages"}
						{if $language.iso_code != $lang_iso}
								{assign var=indice_lang value=$language.id_lang}
								{if isset($lang_rewrite_urls.$indice_lang)}
									<a href="{$lang_rewrite_urls.$indice_lang|escape:'html':'UTF-8'}" title="{$language.name}">
								{else}
									<h4 style="display: inline;">|</h3>
									<a href="{$link->getLanguageLink($language.id_lang)|escape:'html':'UTF-8'}" title="{$language.name}">
								{/if}
									<h4 style="display: inline">{$language.iso_code|regex_replace:"/\s\(.*\)$/":""|upper}</h4>
									<!-- <h3 style="display: inline">{$language.name|regex_replace:"/\s\(.*\)$/":""}</h3> -->
								</a>
					{/if}
				{/foreach}
		</div>
	{/if}
	<!-- /Block languages module -->
