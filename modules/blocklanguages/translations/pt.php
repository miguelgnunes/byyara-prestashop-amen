<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocklanguages}byyara-theme>blocklanguages_d33f69bfa67e20063a8905c923d9cf59'] = 'Bloco de Idioma';
$_MODULE['<{blocklanguages}byyara-theme>blocklanguages_5bc2cbadb5e09b5ef9b9d1724072c4f9'] = 'Adiciona um bloco que permite aos clientes escolher um idioma para o conteúdo da loja.';
$_MODULE['<{blocklanguages}byyara-theme>blocklanguages_5b64a5253f843da2950972747ac9a3b0'] = 'IDIOMA';
