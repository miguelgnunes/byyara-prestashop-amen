{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
	<div class="currencies-container">
		<!-- Block currencies module -->
		{if count($currencies) > 1}
			{* <div id="currencies-block-top" class="languages-block">
				<form id="setCurrency" action="{$request_uri}" method="post">
				<input type="hidden" name="id_currency" id="id_currency" value=""/>
				<input type="hidden" name="SubmitCurrency" value="" />
				{foreach from=$currencies key=k item=f_currency}
					<h4 style="display:inline">{$language.iso_code|regex_replace:"/\s\(.*\)$/":""|upper}</h4>
					{if $cookie->id_currency == $f_currency.id_currency}<h4 style="display:inline">{$f_currency.iso_code}</h4>{/if}
				{/foreach}

				{foreach from=$currencies key=k item=f_currency}
					{if strpos($f_currency.name, '('|cat:$f_currency.iso_code:')') === false}
						{assign var="currency_name" value={l s='%s (%s)' sprintf=[$f_currency.iso_code]}}
					{else}
						{assign var="currency_name" value=$f_currency.name}
					{/if}
					<a href="javascript:setCurrency({$f_currency.id_currency});" rel="nofollow" title="{$currency_name}">
						<h4 style="display: inline">{$currency_name}</h4>
					</a>
				{/foreach}
			</div> *}

			<div id="currencies-block-top" class="languages-block">
				{$eurocode = ""}
				{$dollarcode = ""}
				{$kwanzacode = ""}
				{foreach from=$currencies key=k item=f_currency}
					{if $f_currency.iso_code == "EUR"}
						{$eurocode = $f_currency.id_currency}
					{else if $f_currency.iso_code == "USD"}
						{$dollarcode = $f_currency.id_currency}
					{else}
						{$kwanzacode = $f_currency.id_currency}
					{/if}
				{/foreach}
				{if $cookie->id_currency == $eurocode}
				<span class="selected-currency" style="display: inline; font-size: 10px; font-style: bold">EUR</span>
				{else}
				<a href="javascript:setCurrency({$eurocode});" rel="nofollow" title="{$f_currency.name}"><span style="display: inline; margin-left: 1px; font-size: 10px">EUR</span></a>
				{/if}
				<span style="display: inline">|</span>
				{if $cookie->id_currency == $dollarcode}
				<span class="selected-currency" style="display: inline;">USD</span>
				{else}
				<a href="javascript:setCurrency({$dollarcode});" rel="nofollow" title="{$f_currency.name}"><span style="display: inline; margin-left: 1px; font-size: 10px">USD</span></a>
				{/if}
				<span style="display: inline">|</span>
				{if $cookie->id_currency == $kwanzacode}
				<span class="selected-currency" style="display: inline; ">AOA</span>
				{else}
				<a href="javascript:setCurrency({$kwanzacode});" rel="nofollow" title="{$f_currency.name}"><span style="display: inline; margin-left: 1px; font-size: 10px">AOA</span></a>
				{/if}

				{* {foreach from=$currencies key=k item=f_currency}
					{if $cookie->id_currency == $f_currency.id_currency}
						<span class="selected-currency" style="display: inline; font-size: 10px; font-style: bold">{$f_currency.iso_code}</span>
					{/if}
				{/foreach}
				{foreach from=$currencies key=k item=f_currency}
					{if $cookie->id_currency != $f_currency.id_currency}
				<span style="display: inline">|</span>
						<a href="javascript:setCurrency({$f_currency.id_currency});" rel="nofollow" title="{$f_currency.name}"><span style="display: inline; margin-left: 1px; font-size: 10px">{$f_currency.iso_code}</span></a>
					{/if}
				{/foreach} *}

				{* {$counter = 0}
				{foreach from=$currencies key=k item=f_currency}
					{if $counter != 0}
						<span style="display: inline">|</span>
					{/if}
					{if $cookie->id_currency != $f_currency.id_currency}
					<a href="javascript:setCurrency({$f_currency.id_currency});" rel="nofollow" title="{$f_currency.name}"><span style="display: inline">{$f_currency.sign}</span></a>
					{else}
					<span style="display: inline; font-weight: bold; font-size: 15px">{$f_currency.sign}</span>
					{/if}
					{$counter = $counter + 1}
				{/foreach} *}
			</div>

					{* <div class="current">
						<input type="hidden" name="id_currency" id="id_currency" value=""/>
						<input type="hidden" name="SubmitCurrency" value="" />
						<span class="cur-label">{l s='Currency' mod='blockcurrencies'} :</span>
						{foreach from=$currencies key=k item=f_currency}
							{if $cookie->id_currency == $f_currency.id_currency}<strong>{$f_currency.iso_code}</strong>{/if}
						{/foreach}
					</div>
					<ul id="first-currencies" class="currencies_ul toogle_content">
						{foreach from=$currencies key=k item=f_currency}
							{if strpos($f_currency.name, '('|cat:$f_currency.iso_code:')') === false}
								{assign var="currency_name" value={l s='%s (%s)' sprintf=[$f_currency.name, $f_currency.iso_code]}}
							{else}
								{assign var="currency_name" value=$f_currency.name}
							{/if}
							<li {if $cookie->id_currency == $f_currency.id_currency}class="selected"{/if}>
								<a href="javascript:setCurrency({$f_currency.id_currency});" rel="nofollow" title="{$currency_name}">
									{$currency_name}
								</a>
							</li>
						{/foreach}
					</ul>
				</form>
			</div> *}
		{/if}
		<!-- /Block currencies module -->
	</div>{* currencies-container *}
</div>{* languages and currency container *}
