var inCategoryPage = false;
var inProductPage = false;
var lastClickedFilter;
var normalFontWeight;
if(window.innerWidth < 481){
    var normalFontColor = "#8f9396";
}
else
    var normalFontColor = "#000";


$(document).ready(function() {

            // slide branca mao na testa
            $('#slide-1-1').tooltipster({
            	arrow: false,
            	interactive: true,
            	position: 'bottom-left',
            	delay: 50,
            	theme: 'tooltipster-noir',
            	content: $('<a href="index.php?id_product=13&controller=product" target="_blank"><center><img class="tooltip-img" src="img/p/5/5/55-thickbox_default.jpg"></center><h5 class="tooltip-header">KAKWEJI</h5></a>')
            });
              $('#slide-1-2').tooltipster({
                  arrow: false,
                  interactive: true,
                  position: 'bottom-left',
                  delay: 50,
                  theme: 'tooltipster-noir',
                  content: $('<a href="index.php?id_product=1&controller=product" target="_blank"><center><img class="tooltip-img" src="img/p/3/6/36-large_default.jpg"></center><h5 class="tooltip-header">UNGEJI</h5></a>')
            });
              // slide branca mao no queixo
              $('#slide-2-1').tooltipster({     
                  arrow: false,
                  interactive: true,
                  position: 'bottom-left',
                  delay: 50,
                  theme: 'tooltipster-noir',
                  content: $('<a href="index.php?id_product=2&controller=product" target="_blank"><center><img class="tooltip-img" src="img/p/4/1/41-large_default.jpg"></center><h5 class="tooltip-header">UTANHA</h5></a>')
            });
              // slide branca mao no queixo
              $('#slide-2-2').tooltipster({     
                  arrow: false,
                  interactive: true,
                  position: 'bottom-left',
                  delay: 50,
                  theme: 'tooltipster-noir',
                  content: $('<a href="index.php?id_product=2&controller=product" target="_blank"><center><img class="tooltip-img" src="img/p/3/5/35-home_default.jpg"></center><h5 class="tooltip-header">2ND SLIDE#2</h5></a>')
            });
              $('#slide-3-1').tooltipster({     
                  arrow: false,
                  interactive: true,
                  position: 'bottom-left',
                  delay: 50,
                  theme: 'tooltipster-noir',
                  content: $('<a href="index.php?id_product=8&controller=product" target="_blank"><center><img class="tooltip-img" src="img/p/4/4/44-large_default.jpg"></center><h5 class="tooltip-header">OLUI</h5></a>')
            });
               $('#slide-3-2').tooltipster({     
                  arrow: false,
                  interactive: true,
                  position: 'bottom-left',
                  delay: 50,
                  theme: 'tooltipster-noir',
                  content: $('<a href="index.php?id_product=8&controller=product" target="_blank"><center><img class="tooltip-img" src="img/p/4/4/44-large_default.jpg"></center><h5 class="tooltip-header">OLUI</h5></a>')
            });
            $('#slide-4-1').tooltipster({     
                  arrow: false,
                  interactive: true,
                  position: 'bottom-left',
                  delay: 50,
                  theme: 'tooltipster-noir',
                  content: $('<a href="2-blouse.html" target="_blank"><center><img class="tooltip-img" src="img/p/3/5/35-home_default.jpg"></center><h5 class="tooltip-header">4th slide</h5></a>')
            });




            var self = $("#masonry");
            self.imagesLoaded(function () {
                self.masonry({
                    gutterWidth: 15,
                    isAnimated: true,
                    itemSelector: ".filter-container"
                });
            });

            $(".filter-btn").click(function(e) {
                e.preventDefault();
                if(!normalFontWeight){
                  normalFontWeight = $(this).css("font-weight");
                }

                if(lastClickedFilter != undefined){
                  lastClickedFilter.css("font-weight", normalFontWeight);
                  lastClickedFilter.children().first().css("color", normalFontColor);
                  lastClickedFilter.children().first().removeClass("important-black")
                }
                else{
                  $(".filter-btn").first().css("font-weight", normalFontWeight);
                  $(".filter-btn").first().children().first().css("color", normalFontColor);
                  $(".filter-btn").first().children().first().removeClass("important-black")
                }
                var filter = $(this).attr("data-filter");
                lastClickedFilter = $(this);
                $(this).css("font-weight", "bold");
                console.log("before " + $(this).children().first().css("color"));
                
                $(this).children().first().addClass("important-black");

                console.log("after " + $(this).children().first().css("color"));

                console.log(normalFontColor);
                console.log($(this).children().first());

                self.masonryFilter({
                    filter: function () {
                        if (!filter) return true;
                        return $(this).attr("data-filter") == filter;
                    }
                });
            });
});

window.onload = function(){
  var menuelements = $(".sf-menu > li");
  for(i = 0; i < menuelements.length; i++){
    menuelements.eq(i).css("width", menuelements.eq(i).width());
  }
  var currencyElements = $("#currencies-block-top > span");
  var currencyElements2 = $("#currencies-block-top > a");
  for(i = 0; i < currencyElements.length; i++){
    currencyElements.eq(i).css("width", currencyElements.eq(i).width());
  }
  for(i = 0; i < currencyElements2.length; i++){
    currencyElements2.eq(i).css("width", currencyElements2.eq(i).width());
  }
  if(inCategoryPage && window.innerWidth > 480){
    $(".columns-container").css("height", $(".columns-container").height());
    $(".filter-btn").each(function(){
      $(this).css("width", $(this).css("width"));
    });    
  }
  if(inProductPage) {
    var height = $(".primary_block").children().first().height();
    $(".primary_block").children().eq(1).css("height", height);
    $(".box-info-product").css("position", "absolute").css("bottom", "0");
  }
  if(inProductPage && window.innerWidth > 480){
    
  }
  // $("body").fadeIn();
  $("body").animate({opacity: 1}, 'slow', function(){
      $("body").css('visibility', 'visible');
  });


  $(".zoomWindow").css("left", $("#image-block").width() + 10);

   $("a").removeAttr("title");
}

var footerClick = function(){
      console.log("ehe1");

      if($("#block_various_links_footer").children().first().css("display") != "none") {
            console.log("ehe2");
            var footer = $(".footer-container");
            var arrow = $("#footer-arrow");
            if(footer.css("height") == "40px"){
                  arrow.attr("class", "glyphicon glyphicon-chevron-down");
                  footer.css("height", "auto");
            }
            else{
                  arrow.attr("class", "glyphicon glyphicon-chevron-up");
                  footer.css("height", "40px");
            }
      }
}

// $(".footer-container").click(function(){
// });

var menumobileclick = function(){
      var menuContainer = $("#fullscreen-menu-container");
      var itemsContainer = $("#menu-items-container");
      var menuIcon = $("#menu-icon");
      if(itemsContainer.css("display") == "none"){
            itemsContainer.fadeIn();
            menuContainer.css("z-index", "1");
            menuIcon.animate({  borderSpacing: 90 }, {
                step: function(now,fx) {
                  $(this).css('-webkit-transform','rotate('+now+'deg)'); 
                },
                duration:'slow'
            },'linear');
      }
      else{
            itemsContainer.fadeOut();
            menuContainer.css("z-index", "0");
            menuIcon.animate({  borderSpacing: 0 }, {
                step: function(now,fx) {
                  $(this).css('-webkit-transform','rotate('+now+'deg)'); 
                },
                duration:'slow'
            },'linear');
      }
}

var fixCategoryHeight = function (){
  inCategoryPage = true;
}
var fixBreadcrumbs = function (){
  inProductPage = true;
}